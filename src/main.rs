/// Rust MLDS implementation 
///
/// Copyright (c) 2022 John Clemens <clemej1@umbc.edu>
///
#[allow(unused)]

use log::{debug, info, trace, warn, error, log_enabled};
use serde::{Serialize, Deserialize};
use clap::{ArgEnum, Parser};

use std::path::Path;
use std::fs::File;
//use std::collections::HashMap;

///
/// Available network architectures
/// 
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ArgEnum, Debug, Serialize, Deserialize)]
enum NetworkArch {
    #[clap(name = "GRU")]
    BitMachineGRU,
    #[clap(name = "LSTM")]
    BitMachineLSTM,
    #[clap(name = "DENSE")]
    Dense,
    #[clap(name = "LENET5")]
    ModifiedLeNet5
}

impl Default for NetworkArch {
    fn default() -> NetworkArch {
        NetworkArch::BitMachineGRU
    }
}

/// 
/// Rust implementation of MLDS, the client for MLC@Home
/// 
#[derive(Parser, Debug)]
#[clap(author, version, about)]
struct MLDSArgs {
    /// Network architecture to train
    #[clap(short, long, arg_enum, default_value_t = NetworkArch::BitMachineGRU)]
    arch: NetworkArch,    

    /// Indicate this is a continuation work unit
    #[clap(short, long)]
    cont: bool,

    /// Set verbosity level (can be speficied more than once)
    #[clap(short, long, parse(from_occurrences))]
    verbose: usize,

    /// Maximum number of epochs before exiting
    #[clap(short, long, default_value_t = 128)]
    maxepoch: usize, 

    /// Set the learning rate
    #[clap(short, long, default_value_t = 0.01)]
    lr: f32,

    /// Width of hidden layers
    #[clap(short, long, default_value_t = 12)]
    width: usize,

    /// Number of recurrent layers 
    #[clap(short, long, default_value_t = 4)]
    rlayers: usize, 

    /// Number of backend linear layers 
    #[clap(short, long, default_value_t = 4)]
    blayers: usize,

    /// Batch size 
    #[clap(short('s'), long, default_value_t = 128)]
    batchsize: usize,

    /// Dropout percent
    #[clap(short, long, default_value_t = 0.0)]
    dropout_pct: f32,

    dataset: Option<String>
}

#[derive(Debug, Serialize, Deserialize, Default)]
struct MLDSStatus
{
    dataset_filename: String,
    version: String,
    model_type: NetworkArch,
    batch_size: usize,
    val_loss_threshold: f64,
    learning_rate: f32,
    max_epochs: usize,
    hidden_width: usize, 
    rhidden: usize,
    bhidden: usize,
    input_width: usize, 
    output_width: usize,
    nthreads: usize,
    dropout_pct: f32,
    gpu: bool, 
    train_hist: Vec<f64>,
    val_hist: Vec<f64>
}

impl MLDSStatus {
    fn from_args(args: &MLDSArgs) -> MLDSStatus {
        if files("config").exists() {
            MLDSStatus::load(files("config"))
        } else {
            MLDSStatus {
                dataset_filename: match &args.dataset {
                    Some(x) => String::from(x),
                    _ => String::from("dataset.hdf5")
                },
                version: String::from(option_env!("CARGO_PKG_VERSION").unwrap_or("unknown")),
                model_type: args.arch,
                batch_size: args.batchsize,
                val_loss_threshold: 0.001,
                learning_rate: args.lr,
                max_epochs: args.maxepoch,
                hidden_width: args.width,
                rhidden: args.rlayers,
                bhidden: args.blayers,
                input_width: 0,
                output_width: 0,
                nthreads: 1,
                dropout_pct: args.dropout_pct,
                gpu: false,
                train_hist: Vec::new(),
                val_hist: Vec::new(),
            }
        }
    }

    fn save(&self, filename: &Path) {
        let f = match File::create(filename) {
            Ok(x) => x,
            Err(x) => panic!("Error creating file {}: {}", filename.to_str().unwrap(), x)
        };
        match serde_json::to_writer_pretty(f, &self) {
            Ok(x) => x,
            Err(x) => panic!("Error writing confie file {}: {:?}", filename.to_str().unwrap(), x)
        }
    }

    fn load(filename: &Path) -> MLDSStatus {
        let f = match File::open(filename) {
            Ok(x) => x, 
            Err(x) => panic!("Could not open {}: {}", filename.to_str().unwrap(), x)
        };

        let ret: MLDSStatus = match serde_json::from_reader(f) {
            Ok(x) => x,
            Err(x) => panic!("Error reading config file {}: {}", filename.to_str().unwrap(), x)
        };

        ret
    }
}

//
// Mapping of filenames to needed files 
//
fn files(key: &str) -> &Path {
    match key {
        "dataset" => Path::new("dataset.hdf5"),
        "config" => Path::new("model.cfg"),
        "final" => Path::new("model-final.pt"),
        "input" => Path::new("model-input.pt"),
        "snapshot" => Path::new("snapshot.pt"),
        "best" => Path::new("model-best.pt"),
        _ => {
            warn!("Unknown files key");
            Path::new("")
        }
    }
}

fn main() {
    env_logger::init();
    let args = MLDSArgs::parse();
    let status = MLDSStatus::from_args(&args);

    println!("RMLDS v{}", status.version);
    info!("RMLDS v{}", status.version);

    debug!("args: {:?}", args);
    debug!("status: {:?}", status);

    status.save(Path::new("test.json"));
}
